/**
 * 
 */
package com.springboot.api.gradle.model;

import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;
import org.bson.types.ObjectId;
import java.io.Serializable;
/**
 * @author roger
 *
 */
@Document("Temperatura")
public class Temperatura implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 292472337880583145L;
	private String id = new ObjectId().toString();
	private Date fecha;
	private String hora;
	private Integer valor;
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the fecha
	 */
	public Date getFecha() {
		return fecha;
	}
	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	/**
	 * @return the hora
	 */
	public String getHora() {
		return hora;
	}
	/**
	 * @param hora the hora to set
	 */
	public void setHora(String hora) {
		this.hora = hora;
	}
	/**
	 * @return the valor
	 */
	public Integer getValor() {
		return valor;
	}
	/**
	 * @param valor the valor to set
	 */
	public void setValor(Integer valor) {
		this.valor = valor;
	}
	
	
	

	
}
