/**
 * 
 */
package com.springboot.api.gradle.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.springboot.api.gradle.model.Temperatura;

import reactor.core.publisher.Mono;

/**
 * @author roger
 *
 */
public interface TemperaturaService {

	Map<String, Object> getTemperatura(Date fecha);
	Map<String, Object> getMetricasTemperatura(String horaInicio, String horaFin);
	Mono<Temperatura> getMetricasTemperaturaDia(Date fecha);
	Mono<Temperatura> saveTemperatura(Temperatura temperatura);
	List<Temperatura> getAllTemperaturas();
}
