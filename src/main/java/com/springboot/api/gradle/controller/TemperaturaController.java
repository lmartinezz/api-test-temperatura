package com.springboot.api.gradle.controller;

import java.time.Duration;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.api.gradle.model.Temperatura;
import com.springboot.api.gradle.service.impl.TemperaturaServiceImpl;

import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/temperatura")
public class TemperaturaController {

	@Autowired
	private TemperaturaServiceImpl _temperaturaService;
	
	@GetMapping(value = "/get/{id}", produces = "application/json")	
	public Mono<Temperatura> getTemperatura(@PathVariable ("id") Integer id){
		Date fecha = new Date();
		return _temperaturaService.getMetricasTemperaturaDia(fecha).delayElement(Duration.ofSeconds(4));
	}
	
	@PostMapping(value = "/save", produces = "application/json")	
	public Mono<Temperatura> saveTemperatura(@RequestBody Temperatura temperatura){
		
		return _temperaturaService.saveTemperatura(temperatura);
		
		//return _temperaturaService.getAllTemperaturas();
	}
}
