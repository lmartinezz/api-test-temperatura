/**
 * 
 */
package com.springboot.api.gradle.dao;

import java.util.Date;
import java.util.List;
import java.io.Serializable;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

import com.springboot.api.gradle.model.Temperatura;

import reactor.core.publisher.Flux;
/**
 * @author roger
 *
 */
public interface TemperaturaDao extends ReactiveMongoRepository<Temperatura, Serializable> {
	List<Temperatura> getTemperatura();
	List<Temperatura> getRangoTemperatura(String horaInicio, String horaFin);
	List<Temperatura> getTemperaturaDia(Date fecha);
	void saveTemperatura(Temperatura temperatura);
	Flux<Temperatura> getAllTemperaturas();
	Flux<Temperatura> findByTemperatura(String name);

}
