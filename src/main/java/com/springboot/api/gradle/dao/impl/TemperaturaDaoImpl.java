package com.springboot.api.gradle.dao.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.reactivestreams.Publisher;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.springboot.api.gradle.dao.TemperaturaDao;
import com.springboot.api.gradle.model.Temperatura;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@Repository
public class TemperaturaDaoImpl extends JdbcDaoSupport implements TemperaturaDao {

	public TemperaturaDaoImpl(DataSource dataSource) {
		this.setDataSource(dataSource);
	}
	@Override
	public List<Temperatura> getTemperatura() {
		// TODO Auto-generated method stub
		return null;
		
	}

	@Override
	public List<Temperatura> getRangoTemperatura(String horaInicio, String horaFin) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Temperatura> getTemperaturaDia(Date fecha) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void saveTemperatura(Temperatura temperatura) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public <S extends Temperatura> Mono<S> insert(S entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Temperatura> Flux<S> insert(Iterable<S> entities) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Temperatura> Flux<S> insert(Publisher<S> entities) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Temperatura> Flux<S> findAll(Example<S> example) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Temperatura> Flux<S> findAll(Example<S> example, Sort sort) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Flux<Temperatura> findAll(Sort sort) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Temperatura> Mono<S> save(S entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Temperatura> Flux<S> saveAll(Iterable<S> entities) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Temperatura> Flux<S> saveAll(Publisher<S> entityStream) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Mono<Temperatura> findById(Serializable id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Mono<Temperatura> findById(Publisher<Serializable> id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Mono<Boolean> existsById(Serializable id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Mono<Boolean> existsById(Publisher<Serializable> id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Flux<Temperatura> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Flux<Temperatura> findAllById(Iterable<Serializable> ids) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Flux<Temperatura> findAllById(Publisher<Serializable> idStream) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Mono<Long> count() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Mono<Void> deleteById(Serializable id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Mono<Void> deleteById(Publisher<Serializable> id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Mono<Void> delete(Temperatura entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Mono<Void> deleteAll(Iterable<? extends Temperatura> entities) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Mono<Void> deleteAll(Publisher<? extends Temperatura> entityStream) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Mono<Void> deleteAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Temperatura> Mono<S> findOne(Example<S> example) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Temperatura> Mono<Long> count(Example<S> example) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Temperatura> Mono<Boolean> exists(Example<S> example) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Flux<Temperatura> getAllTemperaturas() {
		// TODO Auto-generated method stub
		logger.debug("::::: Mensaje de prueba :::::::");
		List<Temperatura> listaTemperatura = new ArrayList<Temperatura>();
		return null;
	}
	
	@Query("{ 'fecha': ?0 }")
	@Override
	 public Flux<Temperatura> findByTemperatura(String fecha){
	  return null;
	  }

}
